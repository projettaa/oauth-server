## Description
Développement d’une brique de système d’information de l’ISTIC concernant la partie gestion des stages.

## Architecture
### Architecture sécurisée
![OAuth 2.0](https://i.imgsafe.org/f9edc68b14.png)
### Architecture logique
![Architecture logique](https://i.imgsafe.org/fa00206f0d.png)
#### Couche présentation
Elle correspond à la partie visible et interactive avec les utilisateurs. Développer en angularjs avec spring boot (pour faire ce qu’on appelle reverse proxy).
#### Couche métier
Cette couche englobe deux éléments : 
- Les services métier  Ce sont les composants qui implémentent les différentes fonctionnalités que l’application aura besoin. 
- Les entités métier (Business Object) : Représentent l’ensemble des entités métiers. 
#### Couche persistance (Data layer)
Cette couche assure l’accès aux données du système en offrant les services nécessaires à l’ensemble des actions CRUD.
#### Couche transversale (Cross-Cutting)
Implémente les services techniques transverses comme la sécurité.

## Fonctionnalités
Voici une liste des fonctionnalités principales de l'application :
- Pour l’étudiant :
    * Création d'un compte.
    * Affichage du profil.
    * Création d’un stage.
    * Affichage de tous les stages.
    * Modification / suppression d’un stage.
    * Téléchargement de la convention du stage. 
- Pour l’admin :
    * Réception d'un mail pour valider le compte de l'étudiant.
    * Lister/ajouter/modifier/supprimer un étudiant.
    * Lister/ajouter/modifier/supprimer un enseignant.
    * Lister/ajouter/modifier/supprimer une entreprise.
    * Lister tous les stages. 
    * Affectation d'un enseignant à un stage (validation la demande de convention du stage).


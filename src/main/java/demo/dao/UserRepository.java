package demo.dao;

import demo.models.User;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserRepository extends CrudRepository<User, Integer> {
    User findByUsername(String username);

    @Query("SELECT user From User user join user.role role where role.name='ROLE_ADMIN'")
    List<User> findAllAdmins();
}
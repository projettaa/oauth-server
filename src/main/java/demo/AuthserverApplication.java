package demo;

import demo.dao.RoleRepository;
import demo.dao.UserRepository;
import demo.models.Role;
import demo.models.User;
import demo.services.MailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.oauth2.config.annotation.configurers.ClientDetailsServiceConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configuration.AuthorizationServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableAuthorizationServer;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerEndpointsConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerSecurityConfigurer;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.config.annotation.DefaultServletHandlerConfigurer;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.view.RedirectView;

import javax.servlet.http.HttpServletRequest;
import java.security.Principal;
import java.util.List;

@SpringBootApplication
@Controller
@SessionAttributes("authorizationRequest")
@EnableResourceServer
public class AuthserverApplication extends WebMvcConfigurerAdapter {

    @Autowired
    private RoleRepository roleRepository;
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private MailService    mailService;

    public static void main(String[] args) {
        SpringApplication.run(AuthserverApplication.class, args);
    }

    @RequestMapping("/user")
    @ResponseBody
    public User user(Principal user) {
        return (User) ((OAuth2Authentication) user).getPrincipal();
    }

    @RequestMapping(value = "/register", method = RequestMethod.GET)
    public String getRegister(@RequestParam(name = "redirect_uri", required = false) String redirect_uri, HttpServletRequest request, RedirectAttributes redirectAttributes) {
        if (redirect_uri != null) {
            redirectAttributes.addFlashAttribute("redirect_uri", redirect_uri);

            return "redirect:/register";
        }

        return "register";
    }

    @RequestMapping(value = "/register", method = RequestMethod.POST)
    public RedirectView postRegister(HttpServletRequest request, final RedirectAttributes redirectAttributes) {
        RedirectView redirectView = new RedirectView(request.getContextPath() + "/register");

        Role role = roleRepository.findByName("ROLE_USER");

        if (role == null) {
            role = roleRepository.save(new Role("ROLE_USER"));
        }

        String redirect_uri = request.getParameter("redirect_uri");
        redirectAttributes.addFlashAttribute("redirect_uri", redirect_uri);

        String ine              = request.getParameter("ine");
        String birthDate        = request.getParameter("birthDate");
        String sex              = request.getParameter("sex");
        String adress           = request.getParameter("adress");
        String mail             = request.getParameter("mail");
        String tel              = request.getParameter("tel");
        String firstName        = request.getParameter("firstName");
        String lastName         = request.getParameter("lastName");
        String username         = request.getParameter("username");
        String password         = request.getParameter("password");
        String confirm_password = request.getParameter("confirm_password");

        if (!password.equals(confirm_password)) {
            redirectAttributes.addFlashAttribute("error", "The password and confirmation are different.");
            return redirectView;
        }

        try {
            System.out.println("mot de passe" + confirm_password);
            User user = userRepository.save(new User(ine, firstName, lastName, birthDate, sex, adress, mail, tel, username, password, role));

            if (user == null) {
                redirectAttributes.addFlashAttribute("error", "There was a problem saving the user. Please try later.");
                return redirectView;
            }
        } catch (DataIntegrityViolationException e) {
            redirectAttributes.addFlashAttribute("error", "The user already exists. Please try another username.");
            return redirectView;
        }

        List<User> users = userRepository.findAllAdmins();
        if (!users.isEmpty()) {
            User user = users.get(0);
            mailService.send(user.getMail(), "Inscription", "L'utilisateur " + firstName + " " + lastName + " vient de s'inscrire.");
            redirectAttributes.addFlashAttribute("success", "You are registered. An email was sent to the administrator so as to activate you account.");
        } else {
            redirectAttributes.addFlashAttribute("success", "You are registered.");
        }


        return redirectView;
    }

    @Override
    public void addViewControllers(ViewControllerRegistry registry) {
        registry.addViewController("/login").setViewName("login");
        registry.addViewController("/oauth/confirm_access").setViewName("authorize");
    }

    @Override
    public void configureDefaultServletHandling(DefaultServletHandlerConfigurer configurer) {
        configurer.enable();
    }

    @Configuration
    @Order(-20)
    protected static class LoginConfig extends WebSecurityConfigurerAdapter {

        @Autowired
        private AuthenticationManager authenticationManager;

        @Autowired
        @Qualifier("userDetailsService")
        private UserDetailsService userDetailsService;

        @Bean
        public PasswordEncoder passwordEncoder() {
            return new BCryptPasswordEncoder();
        }

        @Override
        protected void configure(HttpSecurity http) throws Exception {
            // @formatter:off
            http
                    .formLogin()
                    .loginPage("/login")
                    .permitAll()
                    .and()
                    .requestMatchers()
                    .antMatchers("/login", "/oauth/authorize", "/oauth/confirm_access", "/register")
                    .and()
                    .authorizeRequests()
                    .antMatchers("/register").permitAll()
                    .anyRequest().authenticated();
            // @formatter:on
        }

        @Override
        protected void configure(AuthenticationManagerBuilder auth) throws Exception {
            auth.parentAuthenticationManager(authenticationManager);
            auth.userDetailsService(userDetailsService)
                .passwordEncoder(passwordEncoder());
        }
    }

    @Configuration
    @EnableAuthorizationServer
    protected static class OAuth2AuthorizationConfig extends
                                                     AuthorizationServerConfigurerAdapter {

        @Autowired
        private AuthenticationManager authenticationManager;

        @Override
        public void configure(ClientDetailsServiceConfigurer clients) throws Exception {
            clients.inMemory()
                   .withClient("acme")
                   .secret("acmesecret")
                   .authorizedGrantTypes("authorization_code", "refresh_token", "password")
                   .scopes("openid")
                   .autoApprove("openid");
        }

        @Override
        public void configure(AuthorizationServerEndpointsConfigurer endpoints)
                throws Exception {
            endpoints.authenticationManager(authenticationManager);
        }

        @Override
        public void configure(AuthorizationServerSecurityConfigurer oauthServer) throws Exception {
            oauthServer.tokenKeyAccess("permitAll()")
                       .checkTokenAccess("isAuthenticated()");
        }

    }
}
